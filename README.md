#### Installation on Ubuntu via apt-get
##### Installation of Docker.
```bash
sudo apt-get install -y docker.io
```

Testing
```bash
docker --version
systemctl status docker
```
##### Installation of newer version of Docker

###### Windows 7 
[Get docker ce for windows 7](https://docs.docker.com/toolbox/toolbox_install_windows/)

###### Centos
[Get docker ce for centos](https://docs.docker.com/engine/installation/linux/docker-ce/centos/)

###### Ubuntu
[Get Docker CE for Ubuntu](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/)

```bash
sudo apt-get update
```

```bash
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get install docker-ce
```

Testing

```bash
docker --version
systemctl status docker
```

###### Windows

[Docker Community Edition for Windows](https://store.docker.com/editions/community/docker-ce-desktop-windows)

```bash
docker --version
```

#### Dockerizing applications

##### The hello world example

```bash
sudo docker run hello-world

sudo docker ps -a -n 1
```

##### Interactive container

```bash
docker run -it debian bash
```

##### Demonizing programs

```bash
sudo docker build ./daemonize/
```
```text
Sending build context to Docker daemon 2.048 kB
Step 1/2 : FROM debian
 ---> da653cee0545
Step 2/2 : CMD sleep 10
 ---> Using cache
 ---> ed874adf813a
Successfully built ed874adf813a
```

```bash
sudo docker run ed874adf813a
```

#### Container usage
##### Running a webapp in a container

1. Mock container

```bash
sudo docker build ./webapp-mock -t webapp-mock
sudo docker run -d --name webapp-mock-instance webapp-mock 
sudo docker inspect webapp-mock-instance  -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'
```

```text
172.17.0.3
```

```bash
echo "Hello world" | telnet 172.17.0.3 80
```

2. An apache container

```bash
sudo docker run -d -p 80:80 -v "$PWD"/webapp-html:/usr/local/apache2/htdocs/ httpd:2.4
curl localhost
```

3. An apache with php container

```bash
sudo docker run -d -p 80:80 -v "$PWD"/webapp-php:/var/www/html php:7.0-apache
curl localhost
```

##### Investigating a container

```bash
sudo docker run --name debian_inspect debian

sudo docker inspect debian_inspect 
```

##### Port mapping

```bash
sudo docker run -p 8080:80 webapp-mock
```

```bash
echo "hello_world" | telnet localhost 8080
```

##### Viewing the logs

```bash
sudo docker run --name webapp-mock-detached -d -p  8080:80 webapp-mock

echo "Hello logs" | telnet localhost 8080

sudo docker logs webapp-mock-detached
```

##### Looking at processes

```bash
sudo docker run -d --name=docker-sleep-3600 alpine sleep 3600
sudo docker run -d --name=docker-sleep-7200 alpine sleep 7200
sudo docker ps --no-trunc -a
sudo docker ps -a --no-trunc --filter "status=running"
```

##### Stopping and restarting

```bash
sudo docker run -it --name=stopped-container debian bash
> touch /hello-stopped-container
> exit
sudo docker inspect stopped-container -f '{{.State.Status}}'
sudo docker start -ai stopped-container
> ls /hello-stopped-container
```
##### Removing a container

```bash
sudo docker run -d --name alpine-sleep alpine sleep 3600

sudo docker rm alpine-sleep

# Command should fail as running containers cannot be removed
sudo docker stop alpine-sleep

sudo docker inspect alpine-sleep  -f '{{.State.Status}}'

sudo docker rm alpine-sleep

# Command should fail as the container is no longer present
sudo docker inspect alpine-sleep 
```
#### Managing images
##### Listing images

```bash
sudo docker build -t workshop/hello-world workshop/hello-world
sudo docker run workshop/hello-world

sudo docker build -t workshop/hello-world:1 workshop/hello-world-1
sudo docker run workshop/hello-world:1

sudo docker build -t workshop/hello-world:1-beta workshop/hello-world-1-beta
sudo docker run workshop/hello-world:1-beta

sudo docker build -t workshop/hello-world:faulty workshop/hello-world-faulty
sudo docker images workshop/hello-world:faulty

sudo docker images workshop/hello-world

sudo docker rmi workshop/hello-world:faulty
sudo docker images workshop/hello-world:faulty

sudo docker images workshop/hello-world
```
##### Downloading images
```bash
sudo docker pull nginx
sudo docker images nginx
```
##### Finding images
```bash
sudo docker search redis
```
#### Networking of containers
##### Port mapping details

```bash
sudo docker network ls
sudo docker network inspect bridge
ip addr show dev docker0
sudo iptables -t nat -L -n
```

```bash
sudo docker network inspect bridge -f '{{.Containers}}'
sudo docker run -d -p 8080:80 --name network-container alpine nc -l -p 80
sudo docker network inspect bridge -f '{{.Containers}}'
sudo docker exec -it network-container ip addr
sudo docker network inspect bridge

sudo iptables -t nat -L -n
```
##### Linking via user defined networks

```bash
sudo docker network create workshop-network
sudo docker run -d --network=workshop-network --name networked-container-1 alpine sleep 3600
sudo docker run -d --network=workshop-network --name networked-container-2 alpine sleep 3600
sudo docker inspect networked-container-1 -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'
sudo docker exec -it networked-container-2 cat /etc/hosts
sudo docker exec -it networked-container-2 ping -c 1 networked-container-1
```
#### Data in containers

##### Data volumes

```bash
sudo docker volume create --name workshop-volume
sudo docker run -v workshop-volume:/mounted-volume debian ls -alts /mounted-volume
sudo docker run -v workshop-volume:/mounted-volume alpine touch /mounted-volume/hello-world
sudo docker run -v workshop-volume:/mounted-volume debian ls -alts /mounted-volume
```
##### Host directories as data volume

```bash
sudo docker run -v "$PWD/host_volume_directory":/host_directory alpine touch /host_directory/hello-world 
ls ./host_volume_directory
```
##### Host file as data volume

```bash
cat ./host_volume_file
sudo docker run -v "$PWD/host_volume_file":/host_file alpine truncate -s 0 /host_file
cat ./host_volume_file
```

```bash
sudo docker run -d -p 8001:80 -v "$PWD"/host_directory_httpd:/usr/local/apache2/htdocs/ httpd:2.4
curl localhost:8001
echo "Hello edited content!" > ./host_directory_httpd/index.html
curl localhost:8001
```
##### Data volume containers

```bash
sudo docker run -v /workshop-data --name workshop-data-container alpine touch /workshop-data/hello-world
sudo docker inspect workshop-data-container -f '{{.Config.Volumes}}'
sudo docker inspect workshop-data-container -f '{{.Mounts}}'
```
```text
[{volume 54ebd26892c1541b30b63c4789471ffa90019aa71ffcd4545c81948d114570a0 /var/lib/docker/volumes/54ebd26892c1541b30b63c4789471ffa90019aa71ffcd4545c81948d114570a0/_data /workshop-data local  true }]
```
```bash
sudo docker volume inspect 54ebd26892c1541b30b63c4789471ffa90019aa71ffcd4545c81948d114570a0
sudo docker run --volumes-from workshop-data-container alpine ls /workshop-data
```
##### Backup, restore of data volumes

```bash
sudo docker run -v /export-data --name export-data-container alpine touch /export-data/hello-world
sudo docker cp export-data-container:/export-data exported-data
ls exported-data/
sudo docker create -v /import-data --name import-data-container alpine
sudo docker cp exported-data/* import-data-container:/import-data/
sudo docker run --volumes-from import-data-container debian ls /import-data
```
#### Contributing to the ecosystem
##### What is Docker Hub?
hub.docker.com
##### Registering on Docker Hub

##### Command line login

```bash
ls ~/.docker/config.json
sudo docker login
```
```text
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: alexandrustaetu  
Password: 
Login Succeeded
```
```bash
ls ~/.docker/config.json
```
##### Uploading to Docker Hub

```bash
sudo docker build -t alexandrustaetu/docker-hub-upload ./docker-hub-upload
sudo docker push alexandrustaetu/docker-hub-upload
sudo docker rmi alexandrustaetu/docker-hub-upload
sudo docker pull alexandrustaetu/docker-hub-upload
```
##### Private repositories
![Set private repository](./set_private_repository.png "Set private repository")
```bash
sudo docker rmi alexandrustaetu/docker-hub-upload
sudo docker info
sudo docker logout https://index.docker.io/v1/
sudo docker pull alexandrustaetu/docker-hub-upload
```
```text
Using default tag: latest
Error response from daemon: pull access denied for alexandrustaetu/docker-hub-upload, repository does not exist or may require 'docker login'
```
```bash
sudo docker pull alexandrustaetu/docker-hub-upload
```
```bash
Using default tag: latest
latest: Pulling from alexandrustaetu/docker-hub-upload
Digest: sha256:8c03bb07a531c53ad7d0f6e7041b64d81f99c6e493cb39abba56d956b40eacbc
Status: Downloaded newer image for alexandrustaetu/docker-hub-upload:latest
```
##### Automated builds